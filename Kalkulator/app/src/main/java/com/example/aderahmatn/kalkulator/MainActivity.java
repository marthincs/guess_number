package com.example.aderahmatn.kalkulator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText nilai1 = (EditText) findViewById(R.id.textNilai1);
        final TextView viewHasil = (TextView) findViewById(R.id.viewHasil);
        Button tambah = (Button) findViewById(R.id.buttonTambah);
        tambah.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                int n1 = Integer.parseInt(nilai1.getText().toString());

                Random randomNumber = new Random();
                int computerValue = randomNumber.nextInt(100);
                int numberOfTries = 0;
                int success = 0;
                int guess = 0;

                guess = n1;

                if (guess < 1 || guess > 100){
                    viewHasil.setText(String.valueOf("Salah Input"));

                }
                else if (guess == computerValue){
                    success++;
                    viewHasil.setText(String.valueOf("Congratulations you won! Your numbers of tries was: " + numberOfTries + " and the number was: " + computerValue));

                }

                else if(guess < computerValue)  // Checking the original number to the summation
                {

                    viewHasil.setText(String.valueOf("Your guess is too low!"));

                }
                else if (guess > computerValue)
                {
                    viewHasil.setText(String.valueOf("Your guess is too high!"));
                }

            }
        });
    }
}